#ifndef EXTRACT_PACKET_H
#define EXTRACT_PACKET_H
#include <stdio.h>
#include <arpa/inet.h>
#include <netinet/tcp.h>
#include <net/ethernet.h>
#include <netinet/ip.h>

void printPayload(u_char* payload, int payload_size);
void extract_ethernet(const u_char* packet);
void extract_ipheader(const u_char* packet);
struct tcphdr* extract_tcp(const u_char *packet);
void extract_tcp_data(const struct tcphdr* tcph, const u_char *packet, int size);
bool isTcpPacket(const u_char* packet);


#endif // EXTRACT_PACKET_H
