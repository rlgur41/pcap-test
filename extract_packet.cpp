#include "extract_packet.h"


bool isTcpPacket(const u_char* packet)
{
    struct iphdr *iph = (struct iphdr *)(packet + sizeof(struct ethhdr));

    if (iph->protocol != IPPROTO_TCP) {
        return false;
    }

    return true;
}


void printPayload(u_char* payload, int payload_size)
{
    for (int i = 0; i < payload_size; i++) {
        printf("%c", (u_char)payload[i]);
    }
}

void extract_ethernet(const u_char* packet) {
    printf("============== ETHERNET ==============\n");

    struct ethhdr *eth = (struct ethhdr*) packet;

    printf("\nsource mac : ");
    for (int i = 0; i < 6; i++) {
        printf("%.2X ", eth->h_source[i]);
    }

    printf("\ndestination mac : ");
    for (int i = 0; i < 6; i++) {
        printf("%.2X ", eth->h_dest[i]);
    }

    printf("\n================================\n");
}


void extract_ipheader(const u_char* packet) {
    printf("\n============== IP ==============\n");

    struct iphdr *iph = (struct iphdr *)(packet + sizeof(struct ethhdr));

    unsigned int saddr = iph->saddr;
    unsigned int daddr = iph->daddr;

    printf("source ip : %d.%d.%d.%d\n",
           (saddr >> 0 * 8) & 0xFF,
           (saddr >> 1 * 8) & 0xFF,
           (saddr >> 2 * 8) & 0xFF,
           (saddr >> 3 * 8) & 0xFF
          );


    printf("destination ip : %d.%d.%d.%d\n",
           (daddr >> 0 * 8) & 0xFF,
           (daddr >> 1 * 8) & 0xFF,
           (daddr >> 2 * 8) & 0xFF,
           (daddr >> 3 * 8) & 0xFF
          );

    printf("\n================================\n");

}


struct tcphdr* extract_tcp(const u_char *packet)
{
    printf("\n============== TCP ==============\n");
    struct iphdr *iph = (struct iphdr *)(packet + sizeof(struct ethhdr));

    int iphdrlen = iph -> ihl * 4;

    struct tcphdr * tcph = (struct tcphdr *)(packet + iphdrlen + sizeof(struct ethhdr));
    printf("TCP source port : %u\n", ntohs(tcph->source));
    printf("TCP destination port : %u\n", ntohs(tcph->dest));

    printf("\n================================\n");
    return tcph;
}


void extract_tcp_data(const struct tcphdr* tcph, const u_char *packet, int size)
{
    printf("\n============== PAYLOAD ==============\n");

    struct iphdr *iph = (struct iphdr *)(packet + sizeof(struct ethhdr));
    int iphdrlen = iph -> ihl * 4;

    int tcphdrlen = tcph->doff*4;
    int header_size = sizeof(struct ethhdr) + iphdrlen;
    int payload_size = size - header_size;

    u_char* payload = (u_char*)(packet + sizeof(struct ethhdr) + iphdrlen + tcphdrlen);

    printPayload(payload, payload_size);

    printf("\n================================\n");
}
